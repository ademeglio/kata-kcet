/**
 * Stock Checker
 * @author Anthony J. DeMeglio
 */
'use strict';

import logMessage from './js/logger';
import './css/style.css';
import './css/nav.css';
import './favicons/favicons';
import searchByTicker from './js/searchByTicker';
import closeCompanyCard from './js/closeCompanyCard';
import goToProfileDetailsPage from './js/goToProfileDetailsPage';
import getCookie from './js/getcookie';
import renderProfileDetails from './js/renderProfileDetails';
import drawer from './js/drawer';

// Where am I?
const currentOrigin = window.location.origin;
const home = "/";
const profileDetail = "/profile"


// Index Page
if ( (currentOrigin + home) === window.location.href ) {
    logMessage('Welcome to StockChecker!')
    document.getElementById('search-button')
        .addEventListener("click", searchByTicker);

    document.getElementById('card-cancel-btn')
        .addEventListener("click", closeCompanyCard);

    document.getElementById('card-profile-btn')
        .addEventListener("click", goToProfileDetailsPage)
}
// Profile Detail Page
if ( (currentOrigin + profileDetail) === window.location.href ||
    ( currentOrigin + profileDetail + '.html') === window.location.href) {
    const companyProfile = JSON.parse(getCookie());
    logMessage(
        "Details for " + companyProfile.companyName
    )
    renderProfileDetails(companyProfile);
}

// Drawer Actions
const menu = document.getElementsByClassName('menu-bars__container');
const modal = document.getElementsByClassName('drawer__scrim');

menu[0].addEventListener('click', drawer.open);
modal[0].addEventListener('click', drawer.close);


// Needed for Hot Module Replacement
if(typeof(module.hot) !== 'undefined') {    // eslint-disable-line no-undef
    module.hot.accept()                     // eslint-disable-line no-undef
} 