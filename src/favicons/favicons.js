const faviconsContext = require.context(                    // eslint-disable-line no-undef
    '!!file-loader?name=[name].[ext]!.',
    true,
    /\.(svg|png|ico|xml|json|webmanifest)$/
);

faviconsContext.keys().forEach(faviconsContext);

// Needed for Hot Module Replacement
if(typeof(module.hot) !== 'undefined') {    // eslint-disable-line no-undef
    module.hot.accept()                     // eslint-disable-line no-undef
}

// Reference https://medium.com/tech-angels-publications/bundle-your-favicons-with-webpack-b69d834b2f53 Olivier Gonzalez