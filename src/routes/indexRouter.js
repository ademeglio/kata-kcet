var path = require('path');
var express = require('express');
var indexRouter = express.Router();

/* GET home page. */
indexRouter.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname,'/public/index.html'));
});

module.exports = indexRouter;
