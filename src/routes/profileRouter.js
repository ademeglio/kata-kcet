const path = require('path');
const express = require('express');
const profileRouter = express.Router();

/* GET home page. */
profileRouter.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname,'/public/profile.html'));
});

module.exports = profileRouter;
