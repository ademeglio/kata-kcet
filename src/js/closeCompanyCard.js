/**
 * Closes company profile card.
 */
'use strict';

function closeCompanyCard() {
    const sections = document.getElementsByClassName('content__section');
    const companyCard = document.getElementById('company-card');
    const formInputText = document.getElementById('ticker-symbol-input');
    
    companyCard.classList.add('hidden');
    formInputText.value = "";
    for (var i = 0; i < sections.length; i++) {
        sections[i].classList.remove('hidden');
    }
}

module.exports = closeCompanyCard;