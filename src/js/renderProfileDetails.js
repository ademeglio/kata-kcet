/**
 * Render profile detail page 
 * data.  
 */
'use strict';

function renderProfileDetails(companyProfile) {
    
    document.getElementById('p-company-logo').src = companyProfile.image;
    document.getElementById('p-company-name').innerHTML = companyProfile.companyName;
    document.getElementById('p-company-symbol').innerHTML = companyProfile.symbol
    document.getElementById('p-price').innerHTML = `$${companyProfile.price}`
    
    // Set company profile changes and it's formatting.
    const coChanges = companyProfile.changes;
    const coChangesP = document.getElementById('p-changes');
    coChangesP.innerHTML = `(${coChanges})`;
    if (coChanges < 0.0) {
        coChangesP.classList.add('value-is-negative')
    } else if (coChanges > 0.0) {
        coChangesP.classList.add('value-is-positive')
    }  
    // If it's 0.0 leave it as initial.
    document.getElementById('p-ceo').innerHTML = companyProfile.ceo;
    document.getElementById('p-sector').innerHTML = companyProfile.sector;
    document.getElementById('p-industry').innerHTML = companyProfile.industry;
    const coWebsite = document.getElementById('p-website');
    coWebsite.innerHTML = companyProfile.website.toString();
    coWebsite.href = companyProfile.website;
    document.getElementById('p-exchange').innerHTML = `${companyProfile.exchange} (${companyProfile.exchangeShortName})`
    document.getElementById('p-description').innerText = companyProfile.description;
    // Financial Summary Population
    document.getElementById('td-symbol').innerHTML = companyProfile.symbol;
    document.getElementById('td-price').innerHTML = `$${companyProfile.price}`;
    
    const tdChanges = document.getElementById('td-changes');
    tdChanges.innerHTML = `(${coChanges})`;
    if (coChanges < 0.0) {
        tdChanges.classList.add('value-is-negative')
    } else if (coChanges > 0.0) {
        tdChanges.classList.add('value-is-positive')
    }  
    
    document.getElementById('td-beta').innerHTML = companyProfile.beta;
    document.getElementById('td-volume-avrg').innerHTML = companyProfile.volAvg;
    document.getElementById('td-market-cap').innerHTML = companyProfile.mktCap;
    document.getElementById('td-range').innerHTML = `$${companyProfile.range}`; 
    document.getElementById('td-last-div').innerHTML = companyProfile.lastDiv;
    document.getElementById('td-dcf').innerHTML = companyProfile.dcf;
    document.getElementById('td-dcf-diff').innerHTML = companyProfile.dcfDiff;
}

module.exports = renderProfileDetails;