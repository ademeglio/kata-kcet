/**
 * Begins the search process by first validating data.
 * User messages or notifications are displayed when 
 * needed. Used with an onClick() function.
 */
'use strict';

const fetchCoProfileFor = require('../js/fetchCoProfileFor');
const validateTickerSymbol = require('./validateTickerSymbol');
// const userMessage = require('./userMessage');
const createCookie = require('./createCookie');

async function searchByTicker() {
    
    var tickerSymbolInputVal = document.getElementById('ticker-symbol-input').value;
    
    // Validate
    try {
        var validatedTickerSymbol = validateTickerSymbol(tickerSymbolInputVal)
    }
    catch(err) {
        return err;
    }

    // Fetch
    var companyProfileArray = await fetchCoProfileFor(validatedTickerSymbol)
                                    .catch(err => {
                                        return err;
                                    });
    if (!companyProfileArray[0]) {
        return;
    }
    var companyProfile = companyProfileArray[0];
        
    // render card data & create cookie to store companyProfile
    window.setTimeout(renderCardData, 1100 , companyProfile);
    createCookie(companyProfile);
}

function renderCardData(companyProfile) {
    document.getElementById('company-logo').src = companyProfile.image;
    document.getElementById('company-name').innerHTML = companyProfile.companyName;
    document.getElementById('company-ticker').innerHTML = companyProfile.symbol;
    document.getElementById('company-price').innerHTML = `$${companyProfile.price}`;

    const coChanges = companyProfile.changes;
    const coChangesP = document.getElementById('company-changes')
    coChangesP.innerHTML = `(${coChanges})`;
    if (coChanges < 0.0) {
        coChangesP.classList.add('value-is-negative')
    } else if (coChanges > 0.0) {
        coChangesP.classList.add('value-is-positive')
    } 
    // If it's 0.0 leave it as initial.

    const description = companyProfile.description;
    const shortDescription = description.substr(0,100);
    document.getElementById('company-partial-description').innerHTML = `${shortDescription} ....`

    // open modal & store companyProfile in a cookie
    var sections = document.getElementsByClassName('content__section');
    var companyCard = document.getElementById('company-card');
    
    for (var i = 0; i < sections.length; i++) {
        sections[i].classList.toggle('hidden', true);
    }
    
    companyCard.classList.toggle('hidden', false);
}


module.exports = searchByTicker;
