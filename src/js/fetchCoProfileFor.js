/**
 * Fetch company profile from financial Modeling Prep
 * using a Ticker Symbol. Displays User Messages in ui
 * for any errors received.
 * @param {*} tickerSymbol 
 */
'use strict';

const userMessage = require('../js/userMessage');

async function fetchCoProfileFor(tickerSymbol) {
    const ENDPOINT = "https://financialmodelingprep.com/api/v3/profile/";
    const KEY = "?apikey=236012ab9f4f788d89b71081be7b95be";
    // const TEMP_KEY = "?apikey=demo" // only works with AAPL
    userMessage("Searching for " + tickerSymbol + "'s company profile");

    return fetch( ENDPOINT + tickerSymbol + KEY, {
            mode: 'cors'
        } )
        .then(response => {
            if(!response.ok) {
                throw new Error(
                    "Sorry, we're having a connection issue (" + 
                    response.status + " " +
                    response.statusText + ")." +
                    "Please try again in a few minutes."
                );
            }
            return response.json();
        })
        .then(data => {
            const companyProfile = data;
            
            if(companyProfile === undefined || companyProfile.length == 0) {
                const tickerInput = document.getElementById('ticker-symbol-input');
                tickerInput.value = '';
                tickerInput.focus();
                throw new Error('Sorry, no matches found for (' +
                    tickerSymbol + '), please try another ticker symbol.');
            }
            
            setTimeout(closeSnackBar, 1000);
            return companyProfile;
        })
        .catch(error => {
            userMessage(error.message);
            return (error);
        })
}

function closeSnackBar() {
    const snackbarAside = document.getElementById('snackbar');
    snackbarAside.classList.remove('snackbar--open');
    snackbarAside.classList.add('snackbar--closing');
    snackbarAside.classList.remove('snackbar--opening')
    snackbarAside.classList.add('snackbar-close');
}

module.exports = fetchCoProfileFor;
