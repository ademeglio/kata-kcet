/**
 * validation utility
 */
'use strict';

const userMessage = require('./userMessage');

function validateTickerSymbol(tickerSymbol) {
    var validatedTickerSymbol = "";
    // validate before fetching. Ex. Try catching an empty field.
    if(tickerSymbol === "") {
        const errorMessage = "Please enter a ticker symbol to search for. " + 
                       "For now, I can't read your mind!"
        userMessage(errorMessage)
        throw new Error("User tried to search for an empty string")
    }

    validatedTickerSymbol = tickerSymbol.toUpperCase();
    return validatedTickerSymbol;
}

module.exports = validateTickerSymbol;