/**
 * get a cookie with profile information so that we don't
 * have to make multiple trips to the api.
 */
'use strict';

function getCookie() {
    var name = "stock-checker-company-profile=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

 module.exports = getCookie;