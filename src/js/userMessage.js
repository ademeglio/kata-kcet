/**
 * Creates a 'snackbar` on the page delivering
 * a user message.
 * @param {*} message 
 */
'use strict';

async function userMessage(message) {
    const snackbarAside = document.getElementById('snackbar');
    const snackbarMessageDiv = document.getElementById('snackbar-message');
    
    // Set snackbar message
    snackbarMessageDiv.innerHTML = message;

    // Display snackbar
    if(!snackbarAside.classList.contains('snackbar--opening')) {
        snackbarAside.classList.remove('snackbar--closing');
        snackbarAside.classList.remove('snackbar--closed');
        snackbarAside.classList.add('snackbar--opening');
    }
    snackbarAside.classList.add('snackbar--open');

    // Wait 5 seconds, and then close the snackbar if no actions.
    setTimeout( () => {
        snackbarAside.classList.remove('snackbar--opening', 'snackbar--open')
        snackbarAside.classList.add('snackbar--closing');
        snackbarAside.classList.add('snackbar-close');
    }, 5000);
}




module.exports = userMessage;