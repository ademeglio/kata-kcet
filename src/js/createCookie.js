/**
 * Set a cookie with profile information so that we don't
 * have to make multiple trips to the api.
 */
'use strict';

function createCookie(companyProfile) {
    var date = new Date();
    date.setTime(date.getTime() + (24*60*60*1000));
    var expires = "expires=" + date.toUTCString();

    document.cookie = 
        "stock-checker-company-profile=" +
        JSON.stringify(companyProfile)   +
        ";" + expires + ";path=/";
}

 module.exports = createCookie;