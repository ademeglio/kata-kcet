/**
 * Gets profile detail page and loads data
 * from cookie
 */
'use strict';

function goToProfileDetailsPage() {
    const url = "/profile";
    const urlDev = "/profile.html"
    
    if (process.env == 'production') {
        window.location.href = url;
    } else {
        window.location.href = urlDev;
    }
}

module.exports = goToProfileDetailsPage;