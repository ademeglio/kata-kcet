/**
 * Open and close the drawer
 */
'use strict';



const open = () => {
    const drawerContainer = document.getElementsByClassName('drawer__container');
   
    drawerContainer[0].classList.add('drawer__container_animate','drawer__container_open');
    setTimeout( () => {
        drawerContainer[0].classList.add('drawer__container_opening');
    }, 150);
}

const close = () => {
    const drawerContainer = document.getElementsByClassName('drawer__container');
    
    drawerContainer[0].classList.add('drawer__container_closing');
    setTimeout( () => {
        drawerContainer[0].classList.remove('drawer__container_open', 'drawer__container_closing',
            'drawer__container_animate', 'drawer__container_opening');
    }, 100);
}


// TODO Testing

module.exports = {
    open,
    close,
}