/**
 * Module dependencies.
 */
import path from 'path';
import express from 'express';
import cookieParser from 'cookie-parser';
import cors from 'cors';
// Routers
import indexRouter from '../routes/indexRouter.js';
import profileRouter from '../routes/profileRouter.js'

/**
 * Create Application
 */
const app = express()
console.log("PRODUCTION SERVER RUNNING");
/**
 * Application Options and configurations
 */
app.use(express.json(), cors());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


/**
 * Set Routes
 */

app.use(express.static(
  path.join(__dirname, 'public'), 
    { // Options
      dotFiles: "ignore",
      index: false,
    }
  ));
app.use('/', indexRouter);
app.use('/profile', profileRouter);

/**
 * Get port from environment and store in Express.
 */
const PORT = normalizePort(process.env.PORT || 3000);
app.set('port', PORT);

/**
 * Listen on provided port, on all network interfaces.
 */
app.listen(PORT, () => {
    console.log(`App listening to ${PORT}....`);
    console.log('Press Ctrl + C to quit.');
});


/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);
  
    if (isNaN(port)) {
      // named pipe
      return val;
    }
  
    if (port >= 0) {
      // port number
      return port;
    }
  
    return false;
  }