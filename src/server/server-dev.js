/**
 * Module dependencies.
 */

import path from 'path';
import express from 'express';
import webpack from 'webpack';
import cookieParser from 'cookie-parser';
import cors from 'cors';
// Routes
// import indexRouter from '../routes/indexRouter.js';
// import profileRouter from '../routes/profileRouter.js';
// Development only dependencies.
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import config from '../../webpack.dev.config.js';


/**
 * Create Application
 */
const app = express(),
    DIST_DIR = __dirname,
    HTML_FILE1 = path.join(DIST_DIR, 'index.html'),
    HTML_FILE2 = path.join(DIST_DIR, 'profile.html'),
    compiler = webpack(config);

console.log("DEV SERVER RUNNING")

/**
 * Instead of regular server, we are serving files in memory
 * so that files changed in watch mode, the middleware delays
 * requests until compiling has competed.
 * 
 * We also setup hot module reload (HMR) for instanst update
 * to the output.
 */

app.use(webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath
    
}));

app.use(webpackHotMiddleware(compiler));

/**
 * Application Options and configurations
 */
app.use(express.json(), cors());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


/**
 * Routing.
 */



app.get('/', (req, res, next) => {
    compiler.outputFileSystem.readFile(HTML_FILE1, (err, result) => {
        if (err) {
            return next(err);
        }
        res.set('content-type', 'text/html');
        res.send(result);
        res.end();
    })
});

app.get('/profile', function (req, res, next) {
    compiler.outputFileSystem.readFile(HTML_FILE2, (err, result) => {
            if (err) {
                return next(err);
            }
            rest.set('content-type', 'text/html');
            res.send(result);
            res.end();
        }) 
});

/**
 * Get port from environment and store in Express.
 */
const PORT = normalizePort(process.env.PORT || 3000);
app.set('port', PORT);


/**
 * Listen on provided port, on all network interfaces.
 */
app.listen(PORT, () => {
    console.log(`App listening to ${PORT}....`);
    console.log('Press Ctrl + C to quit.');
});


/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);
  
    if (isNaN(port)) {
      // named pipe
      return val;
    }
  
    if (port >= 0) {
      // port number
      return port;
    }
  
    return false;
  }