const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');

module.exports = (env, argv) => {
    const SERVER_PATH = (argv.mode === 'production') ? 
        './src/server/server-prod.js' : 
        './src/server/server-dev.js';
    return ({
        entry: {
            server: SERVER_PATH,
        },
        output: {
            path: path.resolve(__dirname, 'dist/'),
            filename: '[name].js'
        },
        mode: argv.mode,
        target: 'node',
        node: {
            __dirname: false,
            __filename: false,
        },
        externals: [nodeExternals()],
        module: {
            rules: [
                {
                    // Transpiles ES6-8 into ES5
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                }
            ]
        }
    })
};


// Assistance from https://medium.com/@binyamin/creating-a-node-express-webpack-app-with-dev-and-prod-builds-a4962ce51334