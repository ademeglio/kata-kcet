'use strict';

import '@testing-library/jest-dom'
const closeCompanyCard = require('../src/js/closeCompanyCard');

beforeEach( () => {
    document.body.innerHTML = 
    "<section class='content__section hidden'>"                                 +
    "<form id='search-by-ticker-form'>"                                         +
    "   <input id='ticker-symbol-input' type='text' value='AAPL'/>"             +
    "   <button id='search-button' type='button' form='search-by-ticker-form'>" +
    "</form>"                                                                   +
    "</section>"                                                                +
    "<aside class='snackbar snackbar--opening' id='snackbar'>"                  +
        "<div class='snackbar__surface'>"                                       +
            "<div class='snackbar__message' id='snackbar-message'>"             +
                "This is a user message, thank you."                            +
                "I will wrap if I type too much!"                               +
            "</div>"                                                            +
        "</div>"                                                                +
    "</aside>"                                                                  +
    "<aside class='card card_position' id ='company-card'>"                     +
        "<div class='card__overlay card__overlay_position'>"                    +
            "<div class='card__header card__header_position'>"                  +
                "<div class='card-header__profile-logo'>"                       +
                    "<img class='profile-logo' id='company-logo'"               +
                    "src='https://financialmodelingprep.com/images-New-jpg"     +
                         "/AAPL.jpg'>"                                          +
                "</div>"                                                        +
                "<div class='card-header__profile-id"                           +
                    "' card-header__profile-id_position'>"                      +
                    "<h6 class='headline-6' id='company-name'>CompanyName</h6>" +
                    "<p class='subhead' id='company-ticker'>TickerSymbol</p>"   +
                "</div>"                                                        +
                "<div class='card-header__profile-stock-price'>"                +
                    "<p class='profile-numbers' id='company-price'>"            +
                        "$124.808"                                              +
                    "</p>"                                                      +
                "</div>"                                                        +
                "<div class='card-header__profile-changes-percentage'>"         +
                    "<p class='profile-numbers value-is-negative'"              +
                       "id=''>"                                                 +
                       "(-75.04%)"                                              + 
                    "</p>"                                                      +
                "</div>"                                                        +
            "</div>"                                                            +
            "<div class='card__chart card__chart_position'></div>"              +
            "<div class='card__secondary-text card__secondary-text_position'>"  +
                "<p class='body-2' id='company-partial-description'>"           +
                    "Lorem ipsum dolor sit amet, consectetur adipisc company"   + 
                    "description ...."                                          +
                "</p>"                                                          +
            "</div>"                                                            +
            "<div class='card__button-row card__button-row_position'>"          +
                "<button class='btn btn-secondary' id='profileBtn'"             +
                        "onclick=getProfile()>"                                 +
                    "PROFILE"                                                   +
                "</button>"                                                     +
                "<button class='btn btn-secondary'  id='card-cancel-btn'>"      +
                    "CANCEL"                                                    +
                "</button>"                                                     +
            "</div>"                                                            +
        "</div>"                                                                +
    "</aside>"
});

describe("Close company card function", () => {
    test("should close open card and redisplay sections", () => {
        // Setup
        const cancelBtn = document.getElementById('card-cancel-btn')
        const companyCard = document.getElementById('company-card');
        const sections = document.getElementsByClassName('content__section');

        // Execute Click Simulation
        cancelBtn
            .addEventListener("click", closeCompanyCard);
        cancelBtn.click;

        // Assert
        setTimeout( () => {
            expect(companyCard).toHaveClass('hidden');
            expect(sections[0]).not.toHaveClass('hidden');
        }, 2000);
        
    })

    test("should reset form input before redisplaying sections", () => {
        const cancelBtn = document.getElementById('card-cancel-btn');
        
        const inputText = document.getElementById('ticker-symbol-input');
        
        // Execute Click Simulation
        cancelBtn
            .addEventListener("click", closeCompanyCard());
        cancelBtn.click;

        // Assert
        expect(inputText.value).toBe("");
        
    })
});