'use strict';

import '@testing-library/jest-dom';
const goToProfileDetailsPage = require('../src/js/goToProfileDetailsPage');

describe("Go to profile details page function", () => {
    test.only("should redirect to target", () => {
        // setup
        const targetDev = '/profile.html'
        
        // Mock
        global.window = Object.create(window);
        Object.defineProperty(window, 'location', {
            writable: true,
            value: {
                href: ''
            }
        });
        
        // Execute
        goToProfileDetailsPage();

        // Assert
        expect(window.location.href).toBe(targetDev);
    });
});