//__tests__/createCookie.spec.js
'use strict';

const companyProfile = require('../__mocks__/appleProfileMock');
const createCookie = require('../src/js/createCookie');
const getCookie = require('../src/js/getCookie');

describe('Create cookie and get cookie functions', () => {

    test.only('should create cookie for company profile (companyProfile) and get it',
        () => {
            
            // Mock Cookie
            Object.defineProperty(window.document, 'cookie', {
                writable: true,
                value: ''
            });
               
            
            // Create Cookie
            createCookie(companyProfile);

            // Pull data from cookie to test
            var pulledCookie = JSON.parse(getCookie("stock-checker-co-profile"))
            // Assert
            expect(pulledCookie.symbol).toBe("AAPL");
        });
});