
describe('Validate Ticker Symbol', () => {
    const validateTickerSymbol = require('../src/js/validateTickerSymbol.js')
    
    beforeAll( () => {
        document.body.innerHTML = 
            "<aside id='snackbar'>"                 +
                "<div id='snackbar-message'></div>" +
            "</aside>"
    });

    test('should throw an error if empty', () => {
        // setup
        const emptyTickerSymbol = "";
        const error = "User tried to search for an empty string";

        // execute & assert
        expect( () => {
            validateTickerSymbol(emptyTickerSymbol);
        }).toThrow(error);
    });

    test('should update user message for snackbar', () => {
        // setup
        const snackbarMessage = document.getElementById("snackbar-message").innerHTML;
        const emptyTickerSymbol = "";
        const userErMessage = "Please enter a ticker symbol to search for. " + 
                             "For now, I can't read your mind!"
        
        // execute & assert
        try {
            validateTickerSymbol(emptyTickerSymbol)
        } catch (e) {
            expect (snackbarMessage).toEqual(userErMessage);
        }
    });

    /**
     * I was originally going to add additional validation
     * but decided not to since the api simply returns
     * an empty body if it can't find a searched for 
     * ticker symbol. 
     * 
     * Also, since the api searches multiple exchanges,
     * I didn't find any constraints on ticker symbol
     * formatting.
     * https://www.thestreet.com/investing/what-is-a-ticker-symbol-14998382
     */
});