// ../__tests__/userMessage.spec.js
'use strict';

const userMessage = require('../src/js/userMessage');

describe("User message to display snackbar on ui", () => {
    // Setup Document Body
    document.body.innerHTML =
        '<aside class="snackbar snackbar--opening" id="snackbar">'      +
            '<div class="snackbar__surface">'                           +
                '<div class="snackbar__message" id="snackbar-message">' +
                    'This is a user message, thank you.'                +
                    'I will wrap if I type too much!'                   +
                '</div>'                                                +
            '</div>'                                                    +
        '</aside>'

    test("should add message (test message) to snackbar", () => {
        // Setup
        const snackBarMessageDiv = document.getElementById('snackbar-message');
        var testMessage = "test message";
        
        // Execute
        userMessage(testMessage);
        
        // Assert
        expect(snackBarMessageDiv.innerHTML).toEqual(testMessage);
    })

    test("should display snackbar", () => {
        // Setup
        const snackbar = document.getElementById('snackbar');
        var testMessage = "I should be displaying!"

        // Execute
        userMessage(testMessage);

        // Assert
        expect(snackbar.classList.contains('snackbar--open'))
            .toBeTruthy;
        expect(snackbar.classList.contains('snackbar--opening'))
            .toBeTruthy;
        expect(snackbar.classList.contains('snackbar--closing'))
            .toBeFalsy;
    });


    test("should be closed after 5 seconds", () => {
        // Setup
        jest.useFakeTimers()
        var testMessage = "I should not be displaying!"

        // Execute
        userMessage(testMessage);

        /*** Assertions ***/

        // Assert snackbar is open
        expect(snackbar.classList.contains('snackbar--open'))
            .toBeTruthy;
        
        // Assert setTimeout() has initiated 
        // and then waits 8s to close snackbar
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(
            Function),5000
        );

        // Assert snackbar has closed.
        expect(snackbar.classList.contains('snackbar--open'))
            .toBeFalsy;
        expect(snackbar.classList.contains('snackbar--closed'))
        .toBeTruthy;
        
    })
    
});

