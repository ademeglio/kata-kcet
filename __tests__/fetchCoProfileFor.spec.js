// ../__tests__/fetchCoProfileFor.spec.js
'use strict';

const fetchCoProfileFor = require('../src/js/fetchCoProfileFor')
const expectedAppleProfile = require('../__mocks__/appleProfileMock');
    

beforeAll( () => {
    require("whatwg-fetch");

    document.body.innerHTML =
        '<aside class="snackbar snackbar--opening" id="snackbar">'      +
            '<div class="snackbar__surface">'                           +
                '<div class="snackbar__message" id="snackbar-message">' +
                    'This is a user message, thank you.'                +
                    'I will wrap if I type too much!'                   +
                '</div>'                                                +
            '</div>'                                                    +
        '</aside>'                                                      +
        '<input id="ticker-symbol-input" type="text" value="mock">' 
});


describe("Fetch company profile for function", () => {
    afterEach( () => {
        // Disable the mock
        window.fetch.mockRestore();
    })

    test("should return profile object with input (AAPL)", async () => {
        // setup
        const tickerSymbol = "AAPL";

        // Mock fetch
        jest.spyOn(window, "fetch").mockImplementation( () => {
            var fetchResponse = {
                ok: true,
                json: () => Promise.resolve(expectedAppleProfile)
            };
            var fetchResponseObj = Promise.resolve(fetchResponse);
            return fetchResponseObj;
        });

        const jsonObj = await fetchCoProfileFor(tickerSymbol);

        // Assertion
        expect(jsonObj).toMatchObject(expectedAppleProfile);
    });
    
    test("should throw an error for a non-existent profile (aaaa)",
        async () => {
        // setup
        const badTickerSymbol = "aaaa";
        const emptyResponse = [];
        const error = 'Sorry, no matches found for (' +
            badTickerSymbol + '), please try another ticker symbol.';
        
            // Mock fetch
        jest.spyOn(window, "fetch").mockImplementation( () => {
            var fetchResponse = {
                ok: true,   // Financial Modeling returns 200 even if
                            // nothing is found and the body is null.
                json: () => Promise.resolve(emptyResponse)
            };
            var fetchResponse = Promise.resolve(fetchResponse);
            
            return fetchResponse;
        });

        // Assertion
        await expect(fetchCoProfileFor(badTickerSymbol)).resolves.toThrow(error);
    });

    test('should throw an error for !ok response', async () => {
        // setup
        const tickerSymbol = "AAPL"
        const error = /Sorry, we're having a connection issue/

        // Mock fetch
        jest.spyOn(window, "fetch").mockImplementation( () => {
            var fetchResponse = {
                ok: false,
                status: 404,
                statusText: "Not Found",
                json: () => Promise.resolve(expectedAppleProfile)
            };
            var fetchResponseObj = Promise.resolve(fetchResponse);
            return fetchResponseObj;
        });

        // Assertion
        await expect(fetchCoProfileFor(tickerSymbol)).resolves.toThrow(error);
    }); 

    test('should add user message to ui for HTTP errors', async () => {
        // setup
        const snackBarMessageDiv = document.getElementById('snackbar-message');
        const tickerSymbol = "AAPL"
        const error = /Sorry, we're having a connection issue/

        // Mock fetch
        jest.spyOn(window, "fetch").mockImplementation( () => {
            var fetchResponse = {
                ok: false,
                status: 404,
                statusText: "Not Found",
                json: () => Promise.resolve(expectedAppleProfile)
            };
            var fetchResponseObj = Promise.resolve(fetchResponse);
            return fetchResponseObj;
        });

        // Assertion
        await expect(fetchCoProfileFor(tickerSymbol)).resolves.toThrow(error);
        expect(snackBarMessageDiv.innerHTML).toMatch(error);
    })

    test("should add user message to ui for unsuccessful search",
        async () => {
        // setup
        const snackBarMessageDiv = document.getElementById('snackbar-message');
        const badTickerSymbol = "aaaa";
        const emptyResponse = [];
        const error = 'Sorry, no matches found for (' +
            badTickerSymbol + ')';
        
            // Mock fetch
        jest.spyOn(window, "fetch").mockImplementation( () => {
            var fetchResponse = {
                ok: true,   // Financial Modeling returns 200 even if
                            // nothing is found and the body is null.
                json: () => Promise.resolve(emptyResponse)
            };
            var fetchResponse = Promise.resolve(fetchResponse);
            
            return fetchResponse;
        });

        // Assertion
        await expect(fetchCoProfileFor(badTickerSymbol)).resolves.toThrow(error);
        expect(snackBarMessageDiv.innerHTML).toMatch(error);
    })

    

});
