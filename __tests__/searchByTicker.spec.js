//__tests__/searchByTicker.spec.js
'use strict';

import '@testing-library/jest-dom'
const searchByTicker = require('../src/js/searchByTicker.js');

// response
const expectedAppleProfile = require('../__mocks__/appleProfileMock');

beforeEach( () => {
    require("whatwg-fetch");

    document.body.innerHTML =
        "<section class='content__section'>"                                      +
        "<form id='search-by-ticker-form'>"                                         +
        "   <input id='ticker-symbol-input' type='text' value='AAPL'/>"             +
        "   <button id='search-button' type='button' form='search-by-ticker-form'>" +
        "</form>"                                                                   +
        "</section>"                                                                +
        "<aside class='snackbar snackbar--opening' id='snackbar'>"                  +
            "<div class='snackbar__surface'>"                                       +
                "<div class='snackbar__message' id='snackbar-message'>"             +
                    "This is a user message, thank you."                            +
                    "I will wrap if I type too much!"                               +
                "</div>"                                                            +
            "</div>"                                                                +
        "</aside>"                                                                  +
        "<aside class='card card_position hidden' id ='company-card'>"              +
            "<div class='card__overlay card__overlay_position'>"                    +
                "<div class='card__header card__header_position'>"                  +
                    "<div class='card-header__profile-logo'>"                       +
                        "<img class='profile-logo' id='company-logo'"               +
                        "src='https://financialmodelingprep.com/images-New-jpg"     +
                             "/AAPL.jpg'>"                                          +
                    "</div>"                                                        +
                    "<div class='card-header__profile-id"                           +
                        "' card-header__profile-id_position'>"                      +
                        "<h6 class='headline-6' id='company-name'>CompanyName</h6>" +
                        "<p class='subhead' id='company-ticker'>TickerSymbol</p>"   +
                    "</div>"                                                        +
                    "<div class='card-header__profile-stock-price'>"                +
                        "<p class='profile-numbers' id='company-price'>"            +
                            "$124.808"                                              +
                        "</p>"                                                      +
                    "</div>"                                                        +
                    "<div class='card-header__profile-changes-percentage'>"         +
                        "<p class='profile-numbers value-is-negative'"              +
                           "id=''>"                                                 +
                           "(-75.04%)"                                              + 
                        "</p>"                                                      +
                    "</div>"                                                        +
                "</div>"                                                            +
                "<div class='card__chart card__chart_position'></div>"              +
                "<div class='card__secondary-text card__secondary-text_position'>"  +
                    "<p class='body-2' id='company-partial-description'>"           +
                        "Lorem ipsum dolor sit amet, consectetur adipisc company"   + 
                        "description ...."                                          +
                    "</p>"                                                          +
                "</div>"                                                            +
                "<div class='card__button-row card__button-row_position'>"          +
                    "<button class='btn btn-secondary' id='profileBtn'"             +
                            "onclick=getProfile()>"                                 +
                        "PROFILE"                                                   +
                    "</button>"                                                     +
                    "<button class='btn btn-secondary' id='cancelBtn'"              +
                            "onclick='closeAndResetModal()'>"                       +
                        "CANCEL"                                                    +
                    "</button>"                                                     +
                "</div>"                                                            +
            "</div>"                                                                +
        "</aside>"

        // Mock
        jest.spyOn(window, "fetch").mockImplementation( () => {
            var fetchResponse =  {
                ok: true,
                json: () => Promise.resolve(expectedAppleProfile)
            };
            var fetchResponseObj = Promise.resolve(fetchResponse);
            return fetchResponseObj;
        });
});


describe("Search by Ticker Function", () => {
    test("should display company card (modal) when " +
        " entering valid ticker symbol (AAPL)", () => {
        // Setup
        const searchButton = document.getElementById('search-button');
        const companyCard = document.getElementById('company-card');

        // Execute Click Simulation
        searchButton
            .addEventListener("click",searchByTicker);
        searchButton.click;
        
        // Assert Card Displays
        setTimeout( () => {
            expect(companyCard).not.toHaveClass('hidden');
        }, 3000)
        
    });

    test("should populate modal data", async () => {
        // Setup
        const searchButton = document.getElementById('search-button');
        const searchedValue = document.getElementById("ticker-symbol-input").value = "AAPL";
        
        // Execute Click Simulation
        searchButton
            .addEventListener("click",searchByTicker);
        searchButton.click;
        
        // Assert modal fields update
        setTimeout( () => {
            const retrievedValue = document.getElementById('company-ticker').value;
            expect(retrievedValue).toEqual(searchedValue)
        },3000);
    });

    test('should hide main content', async () => {
        // Setup
        const searchButton = document.getElementById('search-button');
        const companyCard = document.getElementById('company-card');
        const sectionContent = document.getElementsByClassName('content__section');

        // Execute click simulation
        searchButton
            .addEventListener("click",searchByTicker);
        searchButton.click;

        // Assert
        setTimeout( () => {
            expect(sectionContent[0]).toHaveClass('hidden');
            expect(companyCard).not.toHaveClass('hidden');
        }, 2000);
    });
});
